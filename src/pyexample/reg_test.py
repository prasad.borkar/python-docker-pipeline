import json

print('Generating test report...')

with open('test_report.json', 'w') as file:
    data = {"test1": "Passed", "test2": "Failed"}
    file.write(json.dumps(data))

print('report created')