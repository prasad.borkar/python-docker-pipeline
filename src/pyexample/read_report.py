import json

print('Starting to read regression report')

with open('test_report.json', 'r') as file:
    data = json.load(file)
    print(data)

print('Data read complete')